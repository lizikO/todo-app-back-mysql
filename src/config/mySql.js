import dotenv from "dotenv";
import Sequelize from "sequelize";
import bcrypt from "bcrypt";

dotenv.config();

const connection = new Sequelize({
  dialect: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "Password",
  database: "todo_application",
});

export default connection;
