import Task from "../models/task.js"; // Import your Sequelize Task model
import { v4 as uuidv4 } from "uuid";

// Get all tasks for a user
export const getAllTasks = async (req, res) => {
  const { userId } = req.body;

  try {
    // Use Sequelize's findAll method to fetch tasks for the specified user
    const tasks = await Task.findAll({ where: { userId: userId } });

    // Return the tasks as JSON
    return res.status(200).json(tasks);
  } catch (error) {
    console.error("Error fetching tasks:", error);
    return res.status(500).json({ message: "Server error" });
  }
};

// Create a new task
export const createTask = async (req, res) => {
  const { todo, userId } = req.body;
  const id = uuidv4();

  try {
    // Use Sequelize's create method to add a new task
    const newTask = await Task.create({
      id: id,
      todo: todo,
      status: false,
      userId: userId,
    });

    console.log("New task added:", newTask);

    // Return the newly created task as JSON
    return res.status(201).json(newTask);
  } catch (error) {
    console.error("Error adding task:", error);
    return res.status(400).json({ message: "Error adding task" });
  }
};

// Delete a task by ID
export const deleteTask = async (req, res) => {
  const { taskId } = req.params;

  try {
    // Use Sequelize's destroy method to delete a task by its ID
    const result = await Task.destroy({ where: { id: taskId } });

    if (result === 1) {
      return res.status(200).send("Task was deleted successfully");
    } else {
      return res.status(404).send("Task not found");
    }
  } catch (error) {
    console.error("Error deleting task:", error);
    return res.status(500).send("An error occurred while deleting the task");
  }
};

// Update a task's status to true
export const updateTaskTrue = async (req, res) => {
  const { taskId } = req.params;

  try {
    // Use Sequelize's update method to update the task status to true
    const [updatedRowsCount] = await Task.update(
      { status: true },
      { where: { id: taskId } }
    );

    if (updatedRowsCount === 1) {
      return res.status(200).send("Task was updated successfully");
    } else {
      return res.status(404).send("Task not found");
    }
  } catch (error) {
    console.error("Error updating task status:", error);
    return res.status(500).send("An error occurred while updating the task");
  }
};

// Update a task's status to false
export const updateTaskFalse = async (req, res) => {
  const { taskId } = req.params;

  try {
    // Use Sequelize's update method to update the task status to false
    const [updatedRowsCount] = await Task.update(
      { status: false },
      { where: { id: taskId } }
    );

    if (updatedRowsCount === 1) {
      return res.status(200).send("Task was updated successfully");
    } else {
      return res.status(404).send("Task not found");
    }
  } catch (error) {
    console.error("Error updating task status:", error);
    return res.status(500).send("An error occurred while updating the task");
  }
};
