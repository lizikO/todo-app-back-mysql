import User from "../models/user.js";
import tasks from "../models/todo.js";
import connection from "../config/mySql.js";
import createUserTableSQL from "../models/user.js";
import createTasksTableSQLL from "../models/todo.js";
import { v4 as uuidv4 } from "uuid";

import bcrypt from "bcrypt";

export const createNewUser = async (req, res) => {
  const { email, password } = req.body;

  try {
    // Check if the user with the given email already exists
    const existingUser = await User.findOne({ where: { email: email } });

    if (existingUser) {
      return res.status(401).send("Account already exists");
    }

    // Hash the password
    const saltRounds = 10; // Number of salt rounds
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    // Create a new user using Sequelize with the hashed password
    const newUser = await User.create({
      email: email,
      password: hashedPassword,
    });

    console.log("New user added:", newUser);
    return res.status(201).json("User created successfully");
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({ message: "Server error" });
  }
};

// export const createNewUser = async (req, res) => {
//   const { email, password } = req.body;
//   const user = await users.find({ email });
//   if (user.length > 0) {
//     return res.status(401).send("Account already exists");
//   }
//   const id = uuidv4();

//   const salt = await bcrypt.genSalt(10);
//   const hashedPassword = await bcrypt.hash(password, salt);

//   try {
//     const newUser = await users.create({
//       id: id,
//       email: email,
//       password: hashedPassword,
//     });
//     console.log(newUser);

//     return res.status(201).json("user created successfully");
//   } catch (error) {
//     return res.status(400).json({ message: "error adding new user" });
//   }
// };

export const logInUser = async (req, res) => {
  try {
    const { email, password } = req.body;
    console.log(email);

    // Check if a user with the given email exists in the "users" table
    const existingUser = await User.findOne({
      attributes: ["id", "email", "password"],
      where: { email: email },
    });

    connection.query(existingUser, email, async (err, results) => {
      if (err) {
        console.error("Error checking user:", err);
        return res.status(500).json({ message: "Server error" });
      }

      if (results.length === 0) {
        return res.status(401).json("Failed to log in");
      }

      const user = results[0];
      const hashedPassword = user.password;

      // Compare the provided password with the hashed password from the database
      bcrypt.compare(password, hashedPassword, (compareErr, passwordMatch) => {
        if (compareErr) {
          console.error("Error comparing passwords:", compareErr);
          return res.status(500).json({ message: "Server error" });
        }

        if (!passwordMatch) {
          return res.status(401).json("Failed to log in");
        }

        // If password matches, you can fetch related tasks, similar to your MongoDB code
        const selectTasksSQL = "SELECT * FROM tasks WHERE userId = ?";
        connection.query(selectTasksSQL, user.id, (taskErr, taskResults) => {
          if (taskErr) {
            console.error("Error fetching tasks:", taskErr);
            return res.status(500).json({ message: "Server error" });
          }

          console.log("Tasks for the user:", taskResults);
          return res.status(200).json(user);
        });
      });
    });
  } catch (error) {
    console.error("Error:", error);
    res.status(400).json(`Error message: ${error}`);
  }
};

// export const logInUser = async (req, res) => {
//   try {
//     const { email, password, id } = req.body;
//     const user = await users.findOne({ email });
//     const hashedPassword = user ? user.password : null;
//     const checkedPassword = bcrypt.compare(password, hashedPassword);

//     if (!checkedPassword) {
//       return response.json("failed to log in");
//     }

//     const task = await tasks.find({ userId: user.id });
//     console.log(task);
//     res.status(201).json(user);
//   } catch (error) {
//     res.status(400).json(`error message: ${error}`);
//   }
// };
