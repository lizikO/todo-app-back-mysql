import connection from "../config/mySql.js";

const createTasksTableSQL = `
  CREATE TABLE IF NOT EXISTS tasks (
    id VARCHAR(36) PRIMARY KEY,
    todo TEXT NOT NULL,
    status BOOLEAN NOT NULL,
    userId VARCHAR(36) NOT NULL
  )
`;

// Create the "tasks" table if it doesn't exist
connection.query(createTasksTableSQL, (err) => {
  if (err) {
    console.error('Error creating "tasks" table:', err);
  } else {
    console.log('Created "tasks" table successfully');
  }
});

// Export the MySQL schema (SQL statement) itself
export default createTasksTableSQL;
