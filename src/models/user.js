import Sequelize from "sequelize";
import connection from "../config/mySql.js";

const User = connection.define(
  "User",
  {
    id: {
      type: Sequelize.STRING(36),
      primaryKey: true,
    },
    email: {
      type: Sequelize.STRING(255),
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING(255),
      allowNull: false,
    },
  },
  {
    timestamps: false, // Disable automatic timestamps
  }
);

export default User;
