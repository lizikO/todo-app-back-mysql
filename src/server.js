import bodyParser from "body-parser";
import express from "express";
import dotenv from "dotenv";

import cors from "cors";
import swaggerMiddleware from "./middlewears/swagger-middlewear.js";
import connection from "./config/mySql.js";
import toDoRouter from "./router/todo-router.js";
import userRouter from "./router/user-router.js";
const app = express();
dotenv.config();

app.use(cors());

app.use(bodyParser.json());

app.use("/api", toDoRouter);
app.use("/api", userRouter);

app.use("/", ...swaggerMiddleware());
app.listen(3001);
